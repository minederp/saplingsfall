/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package SaplingFalling;

import Utilities.Helper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author charles
 */
public class Main extends JavaPlugin implements Listener {
	@Override
    public void onEnable(){
        getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

			@Override
			public void run() {
				saplings.clear();
			}
		}, 200L, 200L);
    }
    
    @Override
    public void onDisable(){
    }
	
	static List<Item> saplings = new ArrayList();
	
	 @EventHandler(priority = EventPriority.HIGHEST)
		public void onItemSpawn(ItemSpawnEvent e){
			Entity entity = e.getEntity();
			if(entity.getType().equals(EntityType.DROPPED_ITEM)){
				Item item = (Item) entity;
				if(item.getItemStack().getType().equals(Material.SAPLING) ){
					saplings.add(item);
					Bukkit.getScheduler().scheduleSyncDelayedTask(this, new SpawnTree(item.getLocation(), item), 100L);
				}
			}
		}

	 @EventHandler(priority = EventPriority.HIGHEST)
	 public void onItemPickup(PlayerPickupItemEvent e){
		 if(e.getItem().getItemStack().getType().equals(Material.SAPLING)){
			 if(saplings.contains(e.getItem())){
				 saplings.remove(e.getItem());
			 }
		 }
	 }
	 
	 int ticks = 0;
	 
	 public class SpawnTree implements Runnable{
		
		Item item;
		Location loc;
		
		public SpawnTree(Location loc, Item item){
			this.loc = loc;
			this.item = item;
		}

			@Override
		public void run() {
			if(item.getLocation().subtract(0, 1, 0).getBlock().getType().equals(Material.GRASS) || item.getLocation().subtract(0, 1, 0).getBlock().getType().equals(Material.DIRT)) {
				if(item.getLocation().getBlock().getType().equals(Material.AIR)){
					if(saplings.contains(item)){
						Location loc1 = item.getLocation();
						loc1.getBlock().setTypeIdAndData(item.getItemStack().getTypeId(), item.getItemStack().getData().getData(), true);
						saplings.remove(item);
						item.remove();
					}
				}
			}
		}
	}
}
